package main

import (
    "strings"
    "regexp"
    "fmt"  
    "net/http"
    "encoding/json"
)

type HookReply struct {
  From string
  FromGravatar string
  Latitude float64
  Longitude float64
  Message string
  Room string
  Callback string
}

type Outgoing struct {
  Message string
}

var weatherMatch *regexp.Regexp = regexp.MustCompile("^[wW]eather in[\\s]+(.*)")
const (
  WeatherApi = "http://api.openweathermap.org/data/2.5/weather?q=%s&units=metric"
  WeatherApiLatLng = "http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&units=metric"
  RegKey = "8hndkwo29c0ABI2n"
)

func weatherHandler(w http.ResponseWriter, r *http.Request) {
  response := r.FormValue("response")
  var hr HookReply
  json.Unmarshal([]byte(response), &hr)

  match := weatherMatch.FindStringSubmatch(hr.Message)
  if (len(match) < 2) {
    return
  }
  fmt.Printf("Match is %s\n", match[1])

  var (
    result *http.Response
    err error
  )
  if (match[1] == "here") {
    result, err = http.Get(fmt.Sprintf(WeatherApiLatLng, hr.Latitude, hr.Longitude))
  } else {
    result, err = http.Get(fmt.Sprintf(WeatherApi, match[1]))
  }
  

  if err != nil {
    fmt.Println(err)
    return
  }
  defer result.Body.Close()
  decoder := json.NewDecoder(result.Body)

  var res map[string]interface{}
  decoder.Decode(&res)
  weather := res["weather"].([]interface{})[0].(map[string]interface{})["description"]
  temp := res["main"].(map[string]interface{})["temp"].(float64)
  fmt.Println(res)
  prettyLocation := res["name"].(string)
  if (strings.TrimSpace(prettyLocation) == "") {
    if match[1] == "here" {
      prettyLocation = fmt.Sprintf("%s's location", hr.From)
    } else {
      prettyLocation = strings.Title(match[1])
    }
  }


  w.Header().Add("Content-Type", "application/json")
  reply := &Outgoing{ fmt.Sprintf("%s at %.2f °C in %s", strings.Title(weather.(string)), temp, prettyLocation)  }
  enc := json.NewEncoder(w)
  enc.Encode(reply)

}

func main() {
    http.HandleFunc("/weather/", weatherHandler)
    // http.Handle("/geo", websocket.Handler(wsGeoParty))
    http.ListenAndServe(":8090", nil)
}
