package backend

import (
    "fmt"
    "time"
    "math"
    "math/rand"
    "io"
    "io/ioutil"
    "crypto/md5"
    "net/http"
    "net/url"
    "encoding/json"
    "regexp"
    "errors"
    // "reflect"
)

const (
    DegToRad = math.Pi/180
    RoomBufferSize = 20
    RandomChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    GravatarUrlFormat = "http://www.gravatar.com/avatar/%s?s=30&default=identicon"
)

var random = rand.New(rand.NewSource(time.Now().UnixNano()))

type Incoming struct {
  from string
  fromGravatar string
  message string
  capture string
  room string
  roomPrettyName string
}

type Event interface {
  From() *ClientInfo
}

type RoomEvent interface {
  Event
  Room() *RoomInfo
}

type MessageEvent interface {
  Event
  Message() string
}

type BaseEvent struct {  
  from *ClientInfo
}
func (be *BaseEvent) From() *ClientInfo {
  return be.from
}

type BaseRoomEvent struct {
  BaseEvent
  room *RoomInfo
}
func (bre *BaseRoomEvent) Room() *RoomInfo {
  return bre.room
}
func NewBaseRoomEvent(from *ClientInfo, room *RoomInfo) BaseRoomEvent {
  return BaseRoomEvent{ BaseEvent{ from } , room }
}

type PrivateMessageEvent struct {
  BaseEvent
  message string
}
func (pme *PrivateMessageEvent) Message() string {
  return pme.message
}
func NewPrivateMessage(from *ClientInfo, message string) PrivateMessageEvent {
  return PrivateMessageEvent{ BaseEvent{ from }, message }
}

type RoomMessageEvent struct {
  BaseRoomEvent
  message string
}
func (rme *RoomMessageEvent) Message() string {
  return rme.message
}
func NewRoomMessageEvent(from *ClientInfo, room *RoomInfo, message string) RoomMessageEvent {
  return RoomMessageEvent{ NewBaseRoomEvent(from, room), message }
}

type JoinEvent struct {
  BaseRoomEvent
}

type LeaveEvent struct {
  BaseRoomEvent
}

type ClientInfo struct {
  name string  
  email string
  gravatarUrl string
  info string
  Latitude float64
  Longitude float64
  clientChan chan Event
  subChan chan *RoomInfo
  active bool
  fileTransferChan chan PendingRequest
}
func (ci *ClientInfo) GetChannel() chan Event {
  return ci.clientChan
}
func (ci *ClientInfo) GetName() string {
  return ci.name
}
func (ci *ClientInfo) GetEmail() string {
  return ci.email
}
func (ci *ClientInfo) GetInfo() string {
  return ci.info
}
func (ci *ClientInfo) GetGravatarUrl() string {
  return ci.gravatarUrl
}
func (ci *ClientInfo) IsActive() bool {
  return ci.active
}
func (ci *ClientInfo) SetActive(newActive bool) {
  ci.active = newActive
}
func (ci *ClientInfo) GetFileTransferChan() chan PendingRequest {
  return ci.fileTransferChan
}


type PubRequest struct {
  from *ClientInfo
  message string
}
func NewPubRequest(client *ClientInfo, message string) PubRequest {
  return PubRequest { client, message }
}

type SubRequest struct {
  room string
  createNew bool
  client *ClientInfo
  initialRoom chan *RoomInfo
}
func NewSubRequest(room string, createNew bool, client *ClientInfo, initialRoom chan *RoomInfo) SubRequest {
  return SubRequest{room, createNew, client, initialRoom}
}

type LeaveRequest struct {
  client *ClientInfo
}
func NewLeaveRequest(client *ClientInfo) LeaveRequest {
  return LeaveRequest{ client }
}

type RoomInfo struct {
  name string
  prettyName string
  subscribe chan SubRequest
  leave chan LeaveRequest
  publish chan PubRequest
  clients map[string]*ClientInfo  
  latitude float64
  longitude float64
}
func (r *RoomInfo) SendMessage(clientInfo *ClientInfo, msg string){
  go func() {
    r.publish <- NewPubRequest(clientInfo, msg)
  }()
}
func (r *RoomInfo) Leave(clientInfo *ClientInfo) {
  r.leave <- NewLeaveRequest(clientInfo)
}
func (r *RoomInfo) GetClients() map[string]*ClientInfo {
  return r.clients
}
func (r *RoomInfo) GetName() string {
  return r.name
}
func (r *RoomInfo) GetPrettyName() string {
  return r.prettyName
}
func (r *RoomInfo) GetLatitude() float64 {
  return r.latitude
}
func (r *RoomInfo) GetLongitude() float64 {
  return r.longitude
}

type Client struct {
  rooms map[string]*RoomInfo
  clients map[string]chan Event
  client *ClientInfo
  handler ClientHandler
  killChan chan interface{}
}
func (c *Client) GetClientInfo() *ClientInfo {
  return c.client
}
func (c *Client) GetRoom(room string) (*RoomInfo, bool) {
  r, present := c.rooms[room]
  return r, present
}

func (c *Client) GetRooms() map[string]*RoomInfo {
  return c.rooms
}

func (c *Client) GetClientNames() []string {
  clientNames := make([]string, len(c.clients))
  i := 0
  for k, _ := range c.clients {
    clientNames[i] = k
    i++
  }
  return clientNames
}

func (c *Client) SendPrivateMessage(to string, msg string, getOtherChannel func(to string, success func(chan Event), fail func(error))) {
  if clientChannel, present := c.clients[to]; present {          
    go func() {
      pMsg := NewPrivateMessage(c.GetClientInfo(), msg)
      clientChannel <- &pMsg
    }()
  } else {
    getOtherChannel(to, func(e chan Event) {
      pMsg := NewPrivateMessage(c.GetClientInfo(), msg)
      e <- &pMsg
    }, func(err error) {
      fmt.Println(err)
    })
  }
  c.handler.onPrivateMessageSent(c, to, msg)
}
func (c *Client) LeaveRoom(room string) {
  if r, present := c.GetRoom(room); present {
    r.Leave(c.GetClientInfo())
  } else {
    fmt.Printf("%s is not in room %s\n", c.client.name, room)
  }
}
func (c *Client) SendRoomMessage(room string, msg string){
  if r, present := c.GetRoom(room); present {          
    r.SendMessage(c.GetClientInfo(), msg)
  } else {
    fmt.Printf("no room %s", room)
  }
}
func (c *Client) GetHandler() ClientHandler {
  return c.handler
}
func (c *Client) Kill() {
  go func() {
    c.killChan <- true
    close(c.client.clientChan)
    close(c.client.subChan)
    close(c.client.fileTransferChan)
    for _, r := range c.rooms {
      r.leave <- NewLeaveRequest(c.client)
    }

  }()
}

type Room struct {
  room *RoomInfo
  archive []Event
}

func (r *Room) GetRoom() *RoomInfo {
  return r.room
}

type Server struct {
  rooms map[string]*Room  
  control chan SubRequest
  hookManager *WebHookManager
}

func (s *Server) GetRooms() map[string]*Room {
  return s.rooms
}

func (s *Server) GetControl() chan SubRequest {
  return s.control
}

type ClientHandler interface {
  onMessage(c *Client, from *ClientInfo, room *RoomInfo, info string)
  onPrivateMessage(c *Client, from *ClientInfo, info string)
  onPrivateMessageSent(c *Client, to string, info string)
  onSubscribe(c *Client, room *RoomInfo)
  onJoin(c *Client, from *ClientInfo, room *RoomInfo)
  onLeave(c *Client, from *ClientInfo, room *RoomInfo)
  onFileTransferRequest(c *Client, from string, filename string, id string)
}

type HookRegistrationRequest struct {
  baseUrl string
  criteria *regexp.Regexp
  callback string
  name string
  email string
  response chan string
}

type HookRegistration struct {
  baseUrl string
  criteria *regexp.Regexp
  clientInfo *ClientInfo
}

type WebHookManager struct {
  rooms map[string]*Room
  hooks map[string]*HookRegistration
  request chan *HookRegistrationRequest
}

type HookReply struct {
  From string
  FromGravatar string
  Latitude float64
  Longitude float64
  Message string
  Room string
}

type HookResponse struct {
  Key string
  Message string
  Room string
}

func (wh *WebHookManager) Register(regRequest *HookRegistrationRequest) (string, error) {  
  // go func() {
    wh.request <- regRequest
    select {
    case key := <- regRequest.response:
      return key, nil
    case <- time.After(5 * time.Minute):
      close(regRequest.response)
      return "", errors.New("Timed out waiting for registration")
    }
    // http.PostForm(regRequest.callback, url.Values{ "key": { key }})

  // }()
}
func (wh *WebHookManager) Match(e MessageEvent) {
  go func() {
    for _, v := range wh.hooks {
      if v.criteria.MatchString(e.Message()) {
        reply, err := json.Marshal(&HookReply{ 
          e.From().name, 
          e.From().GetGravatarUrl(), 
          e.From().Latitude,
          e.From().Longitude,
          e.Message(), 
          e.(RoomEvent).Room().GetName(),
        })
        if err != nil {
          fmt.Println(err)
          continue;
        }
        go func() {
          result, err := http.PostForm(v.baseUrl, url.Values{ "response": { string(reply) }})              
          if (err != nil) {
            fmt.Println(err)
            return
          }
          defer result.Body.Close()
          jsonResponse, err := ioutil.ReadAll(result.Body)
          if (err != nil) {
            fmt.Println(err)
            return
          }
          var hr HookResponse
          json.Unmarshal(jsonResponse, &hr)
          e.(RoomEvent).Room().SendMessage(v.clientInfo, hr.Message)

        }()
      }
    }
  }()
}

func (wh *WebHookManager) Receive(key, hr *HookResponse) {
  if hook, present := wh.hooks[hr.Key]; present {
    if room, roomPresent := wh.rooms[hr.Room]; roomPresent {
      room.room.SendMessage(hook.clientInfo, hr.Message)
    }
  }
}

func NewWebHookManager(rooms map[string]*Room) *WebHookManager {
  
  registration := map[string]*HookRegistration{
    "8hndkwo29c0ABI2n": &HookRegistration{ 
      "http://localhost:8090/weather/", 
      regexp.MustCompile("^[wW]eather in[\\s]+[^\\s]+"), 
      &ClientInfo{ "Weatherman", "weather@gmail.com", gravatarUrl("psychopyko@gmail.com"), "", 0, 0, nil, nil, true, nil },
    },
  }
  whm := &WebHookManager{ rooms, registration, make(chan *HookRegistrationRequest) }
  go func() {
    for {
      select {
      case regRequest := <- whm.request:
        unique := randomString(16)
        for _, present := whm.hooks[unique]; present == true; _, present = whm.hooks[unique] {
          unique = randomString(16)
        }
        whm.hooks[unique] = &HookRegistration{ regRequest.baseUrl, regRequest.criteria, nil }
        go func() {
          regRequest.response <- unique
        }()
      }
    }
  }()
  return whm
}

func RoomService(whm *WebHookManager, name string, latitude float64, longitude float64) *Room {
  subscribe := make(chan SubRequest)
  leave := make(chan LeaveRequest)
  publish := make(chan PubRequest)
  archive := make([]Event, 0, RoomBufferSize)
  formattedAddress := name
  if (latitude != 0 && longitude != 0) {
    var f interface{}
    resp, err := http.Get(fmt.Sprintf("http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false", latitude, longitude))
    defer resp.Body.Close()
    if err != nil {
    }
    body, err := ioutil.ReadAll(resp.Body)
    json.Unmarshal(body, &f);
    formattedAddress = f.(map[string]interface{})["results"].([]interface{})[0].(map[string]interface{})["formatted_address"].(string);
  }
  // fmt.Println(formattedAddress)
  // .(map[string]interface{})["formatted_address"].(string);
  // fmt.Println(reflect.TypeOf(formattedAddress));

  roomInfo := &RoomInfo{ name, formattedAddress, subscribe, leave, publish, make(map[string]*ClientInfo), latitude, longitude }
  r := Room{ roomInfo, archive }
  broadcast := func(e Event, record bool) {
    if record {
      if len(r.archive) + 1 > cap(r.archive) {
        copy(r.archive[0:], r.archive[1:])
        r.archive[len(r.archive)-1] = e
      } else {
        r.archive = append(r.archive, e)
      }
    }
    for _, client := range r.room.clients {
      go func(client *ClientInfo){
        client.clientChan <- e
      }(client)
    }
  }
  go func() {
    for {
      select {
      case subRequest := <- subscribe:
        fmt.Printf("%s has joined room %s\n", subRequest.client.name, name)
        broadcast(&JoinEvent{ NewBaseRoomEvent(subRequest.client, roomInfo) }, false)
        r.room.clients[subRequest.client.name] = subRequest.client
        go func() {
          subRequest.client.subChan <- r.room
          for _, event := range r.archive {
            go func(event Event){
              subRequest.client.clientChan <- event
            }(event)
          }
        }()
      case leaveRequest := <- leave:        
        delete(r.room.clients, leaveRequest.client.name)
        // fmt.Sprintf("%s has left room %s", leaveRequest.client.name, name)
        broadcast(&LeaveEvent{ NewBaseRoomEvent(leaveRequest.client, roomInfo) }, false)
      case pubRequest := <- publish:
        pubRequest.from.SetActive(true)
        e := NewRoomMessageEvent(pubRequest.from, roomInfo, pubRequest.message)                
        go func() {
          whm.Match(&e)
        }()
        broadcast(&e, true)                
        //fmt.Printf("Broadcasted pub request %v\n", e)
      }
    }
  }()
  return &r
}

func Distance(lat1 float64, lon1 float64, lat2 float64, lon2 float64) float64{
  // based on http://www.movable-type.co.uk/scripts/latlong.html
  earthRadius := 6371.0; // km
  dLat := (lat2-lat1) * DegToRad;
  dLon := (lon2-lon1) * DegToRad;
  lat1 = lat1 * DegToRad
  lat2 = lat2 * DegToRad

  a := math.Sin(dLat/2) * math.Sin(dLat/2) +
          math.Sin(dLon/2) * math.Sin(dLon/2) * math.Cos(lat1) * math.Cos(lat2); 
  var c = 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a)); 
  var d = earthRadius * c;
  return d

}

func md5Hash(input string) string {
  h := md5.New()
  io.WriteString(h, input)
  return fmt.Sprintf("%x", h.Sum(nil))
}

func gravatarUrl(email string) string {
  gravatarHash := md5Hash(email)
  return fmt.Sprintf(GravatarUrlFormat, gravatarHash)
}

func ServerService() *Server {
  rooms := make(map[string]*Room)
  control := make(chan SubRequest)
  hookManager := NewWebHookManager(rooms)
  s := &Server{ rooms, control, hookManager }
  go func(){
    for {
      select {
      case cmd := <- control:
        latitude := cmd.client.Latitude
        longitude := cmd.client.Longitude
        fmt.Printf("%s registered with latitude %f and longitude %f\n", cmd.client.name, latitude, longitude)
        subscribed := false
        if (cmd.room == "") {
          var r *Room
          for _,v := range rooms {
            if (v.room.latitude != 0 && v.room.longitude != 0 && !cmd.createNew) {
              d := Distance(latitude, longitude, v.room.latitude, v.room.longitude)
              if (d < 1.0) {
                subscribed = true
                v.room.subscribe <- cmd
                r = v
                break
              }
            }          
          }
          if !subscribed && latitude != 0 && longitude != 0 {
            room := md5Hash(fmt.Sprintf("%.4f-%.4f", latitude, longitude))
            r = RoomService(hookManager, room, latitude, longitude)
            rooms[room] = r              
            r.room.subscribe <- cmd
          } 
          go func() {
            cmd.initialRoom <- r.room
          }()
        } else {
          if room, ok := rooms[cmd.room]; !ok {
            r := RoomService(hookManager, cmd.room, 0, 0)
            rooms[cmd.room] = r
            r.room.subscribe <- cmd
          } else {
            room.room.subscribe <- cmd
          }
        }
      }
    }
  }()
  return s
}

func ClientService(name string, email string, info string, handler ClientHandler, latitude float64, longitude float64) *Client {  
  clientChan := make(chan Event)
  subResultChan := make(chan *RoomInfo)
  rooms := make(map[string]*RoomInfo)
  fileTransferChan := make(chan PendingRequest)
  killChan := make(chan interface{})
  clientInfo := &ClientInfo{name, email, gravatarUrl(email), info, latitude, longitude, clientChan, subResultChan, true, fileTransferChan}
  c := &Client{ rooms, make(map[string]chan Event), clientInfo, handler, killChan }
  go func() {
    for {
      select {
        case e := <- clientChan:
          if (e.From() == c.client) {
            c.client.SetActive(true)
          }
          switch eType := e.(type) {
            case *JoinEvent:              
              handler.onJoin(c, e.From(), e.(*JoinEvent).Room())
            case *LeaveEvent:
              handler.onLeave(c, e.From(), e.(*LeaveEvent).Room())
            case *RoomMessageEvent:
              handler.onMessage(c, e.From(), e.(*RoomMessageEvent).Room(), e.(*RoomMessageEvent).Message())
            case *PrivateMessageEvent:
              handler.onPrivateMessage(c, e.From(), e.(*PrivateMessageEvent).Message())
            default:
              fmt.Printf("Unexpected type %T %+v\n", eType, e)
          }
        case roomInfo := <- subResultChan:
          c.client.SetActive(true)
          rooms[roomInfo.name] = roomInfo
          handler.onSubscribe(c, roomInfo)
        case ftRequest := <- fileTransferChan:
          handler.onFileTransferRequest(c, ftRequest.from, ftRequest.name, ftRequest.id)
        case <- killChan:
          close(killChan)
          return
      }
    }
  }()
  return c
}

type PendingRequest struct {
  id string
  from string
  name string
}
func NewPendingRequest(id string, from string, name string) PendingRequest {
  return PendingRequest{ id, from, name }
}


func randomString(size int) string {
  randomSlice := make([]byte, size)
  for i := 0; i < size; i++ {
    randomSlice[i] = RandomChars[random.Intn(len(RandomChars))]    
  }
  return string(randomSlice)
}

type RegistrationRequest struct {
  inChannel chan []byte
  fileName string
  response chan string  
}

type TransferManager struct {
  pendingTransfers map[string](chan []byte)
  pendingTransferNames map[string]string
  registration chan RegistrationRequest
  deleteChan chan string
}

func (tm *TransferManager) Register(fileName string, inChannel chan []byte) string {
  response := make(chan string)
  tm.registration <- RegistrationRequest{ inChannel, fileName, response }
  id := <- response
  return id
}

func (tm *TransferManager) GetChannel(id string) chan []byte {
  if pendingTransfer, ok := tm.pendingTransfers[id]; ok {
    return pendingTransfer
  } else {
    return nil
  }  
}


func (tm *TransferManager) GetFileName(id string) string {
  if fileName, ok := tm.pendingTransferNames[id]; ok {
    return fileName
  } else {
    return "unknown"
  }  
}

func (tm *TransferManager) Finish(id string) {
  go func() {
    tm.deleteChan <- id
  }()
}

func NewTransferManager() *TransferManager {
  requestChan := make(chan RegistrationRequest)
  deleteChan := make(chan string)
  tm := &TransferManager{ make(map[string](chan[]byte)), make(map[string]string), requestChan, deleteChan }  
  go func() {
    for {
      select {
      case request := <- requestChan:            
        id := randomString(8)
        for found := true; found == true; id = randomString(8) {
          found = false
          for k, _ := range tm.pendingTransfers {
            if id == k {
              found = true
              break
            }
          }
        }
        tm.pendingTransfers[id] = request.inChannel
        tm.pendingTransferNames[id] = request.fileName
        request.response <- id
      case deleteRequestId := <- deleteChan:
        delete(tm.pendingTransfers, deleteRequestId)
        delete(tm.pendingTransferNames, deleteRequestId)            
      }
    }
      
  }()
  return tm
}

type BufferingHandler struct {
  roomBuffers map[string][]string
  privateBuffers map[string][]string
  events []map[string]string
  eventsChan chan map[string]string
  retrieveEventsChan chan chan []map[string]string
}
func (bh *BufferingHandler) appendToRoomBuffer(room string, output string) {
  if roomBuffer, present := bh.roomBuffers[room]; present {
    if len(roomBuffer) + 1 > RoomBufferSize {
      copy(roomBuffer[0:], roomBuffer[1:])
      roomBuffer[len(roomBuffer)-1] = output
    } else {
      roomBuffer = append(roomBuffer, output)
    }
    bh.roomBuffers[room] = roomBuffer
  } else {
    bh.roomBuffers[room] = []string{ output }
  }
}

func (bh *BufferingHandler) appendToPrivateBuffer(name string, output string) {
  if privateBuffer, present := bh.privateBuffers[name]; present {
    bh.privateBuffers[name] = append(privateBuffer, output)
  } else {
    bh.privateBuffers[name] = []string{ output }
  }
}
func (bh *BufferingHandler) onMessage(c *Client, from *ClientInfo, room *RoomInfo, info string) {
  output := fmt.Sprintf("<%s> %s\n", from.name, info) 
  bh.appendToRoomBuffer(room.name, output)
  event := map[string]string{ 
    "type":"message",
    "from":from.name,
    "fromGravatar":from.gravatarUrl,
    "room":room.name,
    "message":info,
  }
  go func() {
    bh.eventsChan <- event
  }()
}
func (bh *BufferingHandler) onPrivateMessage(c *Client, from *ClientInfo, info string) {
  output := fmt.Sprintf("<%s> %s\n", from.name, info) 
  bh.appendToPrivateBuffer(from.name, output)
  event := map[string]string{ 
    "type":"privateMessage",
    "from":from.name,
    "fromGravatar":from.gravatarUrl,
    "message":info,
  }
  go func() {
    bh.eventsChan <- event
  }()
}
func (bh *BufferingHandler) onPrivateMessageSent(c *Client, to string, info string) {
  output := fmt.Sprintf("<%s> %s\n", c.client.name, info) 
  bh.appendToPrivateBuffer(to, output)
  event := map[string]string{ 
    "type":"privateMessageSent",
    "from":c.client.name,
    "fromGravatar":c.client.gravatarUrl,
    "to":to,
    "message":info,
  }
  go func() {
    bh.eventsChan <- event
  }()
}
func (bh *BufferingHandler) onSubscribe(c *Client, room *RoomInfo) {
  output := fmt.Sprintf("You have joined room #%s\n", room.prettyName) 
  bh.appendToRoomBuffer(room.name, output)
  event := map[string]string{ 
    "type":"subscribe",
    "room":room.name,
    "roomPrettyName":room.prettyName,
  }
  go func() {
    bh.eventsChan <- event
  }()
}
func (bh *BufferingHandler) onJoin(c *Client, from *ClientInfo, room *RoomInfo) {
  output := fmt.Sprintf("%s has joined room #%s\n", from.name, room.name) 
  bh.appendToRoomBuffer(room.name, output)
  event := map[string]string{ 
    "type":"join",
    "from": from.name,
    "fromGravatar":from.gravatarUrl,
    "fromInfo":from.info,
    "room":room.name,
    "roomPrettyName":room.prettyName,
  }
  go func() {
    bh.eventsChan <- event
  }()
}
func (bh *BufferingHandler) onLeave(c *Client, from *ClientInfo, room *RoomInfo) {
  output := fmt.Sprintf("%s has left room #%s\n", from.name, room.name) 
  bh.appendToRoomBuffer(room.name, output)
  event := map[string]string{ 
    "type":"leave",
    "from": from.name,
    "fromGravatar":from.gravatarUrl,
    "room":room.name,
    "roomPrettyName":room.prettyName,
  }
  go func() {
    bh.eventsChan <- event
  }()
}
func (bh *BufferingHandler) onFileTransferRequest(c *Client, from string, fileName string, id string) {
  output := fmt.Sprintf("%s wants to send you the file %s with id %s \n", from, fileName, id) 
  bh.appendToPrivateBuffer(from, output)
  event := map[string]string{ 
    "type":"fileTransferRequest",
    "from": from,
    "filename":fileName,
    "id": id,
  }
  go func() {
    bh.eventsChan <- event
  }()

}

func (bh *BufferingHandler) GetBufferForRoom(room string) []string {
  buffer, present := bh.roomBuffers[room]
  if present {
    return buffer
  } else {
    return make([]string, 0)
  }
}
func (bh *BufferingHandler) GetPrivateBuffer(name string) []string {
  buffer, present := bh.privateBuffers[name]
  if present {
    return buffer
  } else {
    return make([]string, 0)
  }
}
func (bh *BufferingHandler) GetEvents(response chan []map[string]string) {
  bh.retrieveEventsChan <- response
}

func NewBufferingHandler() *BufferingHandler {
  bh := &BufferingHandler{ make(map[string][]string), make(map[string][]string), make([]map[string]string, 0), make(chan map[string]string), make(chan chan []map[string]string) }
  go func() {
    for {
      select {
      case event := <- bh.eventsChan:
        bh.events = append(bh.events, event)
      case response := <- bh.retrieveEventsChan:        
        response <- bh.events
        bh.events = make([]map[string]string, 0)
      }
    }
  }()
  return bh
}