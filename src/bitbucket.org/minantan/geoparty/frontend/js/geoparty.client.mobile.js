if (!GeoParty || !GeoParty.backend) { throw new Error("GeoParty Client requires GeoParty backend") }

GeoParty.client = {
  makeClient: function(nickName, email, tagLine, latitude, longitude) {
    var createOrUpdate = function(obj, key, item) {
      if (!(key in obj)) {
        obj[key] = [ item ];
      } else {
        obj[key].push(item);
      }            
    }


    var rooms = {},
        roomNames = {},
        privates = {},
        currentRoom = null,
        currentClient = null,
        currentInterval = null,
        transfers = {};

    var initListComponent = function(container, primaryList, itemMaker, itemFinder, client, allowLastRoomLeave) {
      var rooms = [],
          badges = {};       
          mainBadge = 0;   
      var roomContainer = $(container),
        roomList = $(primaryList),
        roomContainerButton = roomContainer.find("button.navbar-btn");

      roomContainer.hide();

      var makePrimaryItem = function(room) {
        var item = itemMaker(room, client);
        item.addClass("selected");
        return item;
      }

      return {
        get: function() {
          return rooms;
        },
        has: function(room) {
          return rooms.indexOf(room) != -1;
        },
        add: function(room, doNotJoin) {              
          roomContainer.show();

          if (!this.has(room)) {
            var newRoom = itemMaker(room, client);
            roomList.append(newRoom);
            rooms.push(room);
          }
          if (doNotJoin) {
            if (room == this.selected()) {
              return;
            }
            var foundRoom = itemFinder(roomList, room).parent();
            foundRoom.detach()
            roomList.find("li").first().after(foundRoom);
            // move room to after first
          } else {
            this.select(room);
            delete badges[room];
          }
        },
        remove: function(room) {
          var roomIndex = rooms.indexOf(room);
          if (roomIndex == -1) {
            console.log("Cannot leave a room you are not in");
            return;
          }
          // can't leave the last room!
          if (rooms.length == 1 && !allowLastRoomLeave) {
            console.log("can't leave the last room!");
            return;
          }

          itemFinder(roomList, room).parent().remove();
          rooms.splice(rooms.indexOf(room), 1);
          if (rooms.length == 0) {
            roomContainerButton.removeClass("selected");
            roomContainer.hide();
            return;
          } else {
            this.select(this.selected());
          }

          delete badges[room];
        },
        select: function(room) {

          var roomIndex = rooms.indexOf(room);
          if (roomIndex == -1) {
            console.log("Cannot select a room you are not in");
            return;
          }

          roomContainerButton.addClass("selected");
          roomList.find("li.active").removeClass("selected");
          var newSelectedItem = itemFinder(roomList, room),
            newSelectedListItem = newSelectedItem.parent();

          newSelectedListItem.addClass("selected");
          newSelectedListItem.detach();
          roomList.prepend(newSelectedListItem);

          rooms.splice(roomIndex, 1);
          rooms.unshift(room);
        },
        selected: function() {
          if (rooms.length > 0) {
            return rooms[0];
          } else {
            return null;
          }
        },
        loseFocus: function() {
          roomList.find("li.active").removeClass("selected");
          roomContainerButton.removeClass("selected")
        },
        incBadge: function(room) {
          if (room in badges) {
            badges[room]++;
          } else {
            badges[room] = 1;            
          }
          var badge = itemFinder(roomList, room).find(".badge");
          badge.text(badges[room]);
          badge.show();
          badge.prev('.glyphicon').hide();
          mainBadge++;
          var mainBadgeElt = roomContainer.find(".dropdown-toggle .badge");
          mainBadgeElt.prev('.glyphicon').hide();
          mainBadgeElt.text(mainBadge);
          mainBadgeElt.css("display", "inline-block");
        },
        clearBadge: function(room){
          if (room in badges) {
            mainBadge -= badges[room];
            var mainBadgeElt = roomContainer.find(".dropdown-toggle .badge");
            mainBadgeElt.prev('.glyphicon').show();
            mainBadgeElt.text("");
            mainBadgeElt.hide();
          }
          delete badges[room];
          var badge = itemFinder(roomList, room).find(".badge");
          badge.text("");
          badge.hide();
          badge.prev('.glyphicon').show();
        }
      }
    }

    var makeRoomItem = function(room, client) {
      var listItem = $("<li></li>"),
          roomPrettyName = roomNames[room],
          link = $("<a href='#' id='room-" + room + "' title='" + roomPrettyName + "'><span class='glyphicon glyphicon-map-marker'></span> <span class='badge'></span>" + roomPrettyName + "</a>");

      link.click(function(e) {
        client.changeRoom(room);
      });              
      listItem.append(link);
      // listItem.append("blah blah");

      return listItem;
    }

    var roomFinder = function(context, room) {
      return context.find("#room-" + room);
    }

    var makeFileItem = function(fileId, client) {
      var transferEvent = transfers[fileId];
      var listItem = $("<li></li>"),          
          link = $("<a href='/download/" + transferEvent.id + "' id='file-" + transferEvent.id + "'><span class='glyphicon glyphicon-file'></span>  " + transferEvent.filename + " from " + transferEvent.from + "</a>");
      link.click(function(e){
        client.fileTransferComponent.clearBadge(fileId);
        client.fileTransferComponent.remove(fileId);
        delete transfers[fileId];
      });
      listItem.append(link);

      return listItem;
    }

    var fileFinder = function(context, fileId) {
      return context.find("#file-" + fileId);
    }

    var makePrivateItem = function(clientName, client) {
      var listItem = $("<li></li>"),
          link = $("<a href='#' id='client-" + clientName + "'><span class='glyphicon glyphicon-user'></span><span class='badge'></span>" + clientName + "</a>");

      link.click(function(e) {
        var clientItem = $("li[data-client-name=" + clientName + "]");
          gravatar = clientItem.find(".gravatar"),
          tagLine = clientItem.find(".tagLine"),
          userDistance = clientItem.find(".userDistance");
        var clientInfo = {
          name: clientName, 
          gravatar: gravatar.attr("src"),
          tagLine: tagLine.text() || "",
          distance: userDistance.text() || ""
        };
        client.changeToPrivateMsg(clientName, clientInfo);
        e.preventDefault();
      });
      listItem.append(link);

      return listItem;
    }

    var privateFinder = function(context, client) {
      return context.find("#client-" + client);
    }

    var makeClientElt = function(clientInfo, client) {
      // "<img src='" + gravatar + "' class='gravatar pull-left' > "
      var listItem = $("<li data-client-name='" + clientInfo.name + "' class='dropdown media clearfix'></li>");
      var gravatarItem = $("<img src='" + clientInfo.gravatar + "' class='gravatar pull-left'>")
      if(clientInfo.name != nickName) {
        listItem.addClass('dropdown');

        // var dropdownTrigger = $("<a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-cog'></span></a>");

        var dropdownTrigger = $("<a class='dropdown-toggle pull-left' data-toggle='dropdown' href='#'></a>");
        dropdownTrigger.append(gravatarItem);
        listItem.prepend(dropdownTrigger);

        var dropdownMenu = $("" +
          "<ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>" +
            "<li><a href='#' id='client-private-" + clientInfo.name + "'><span class='glyphicon glyphicon-user'></span> Private chat</a></li>" +
            "<li><a href='#' id='client-send-" + clientInfo.name + "'><span class='glyphicon glyphicon-upload'></span> Send file</a></li>" +
          "</ul>");
        listItem.append(dropdownMenu);
        listItem.find("#client-private-" + clientInfo.name).click(function(e) {
          client.startPrivateMsg(clientInfo.name, clientInfo);
          e.preventDefault();
        });
        listItem.find("#client-send-" + clientInfo.name).click(function(e) {
          $('#uploadTarget').val(clientInfo.name);
          $('#upload').modal();
          e.preventDefault();
        });
      } else {
        gravatarItem.addClass("pull-left")
        listItem.append(gravatarItem);
      }
      listItem.append($("" +
        "<div class='media-body'>" +
          "<div style='display:none' class='userDetail'>" +
            "<span class='media-heading'><strong>" + clientInfo.name + "</strong></span><br>" +
            "<div class='tagLine'>" + (clientInfo.tagLine ? clientInfo.tagLine : "") + "</div>" +
          "</div>" +
        "</div>"));
      if (clientInfo.name != nickName && clientInfo.distance) {
        var distance = $("<div class='label label-default'><span class='userDistance'>" + clientInfo.distance + "</span> km <span class='glyphicon glyphicon-globe'></span></div>");
        listItem.find('.userDetail').append(distance);
      } else {
        var distance = $("<div class='label label-primary'><span class='userDistance' style='line-height:1.4'>You</span></div>");
        listItem.find('.userDetail').append(distance);
      }
      return listItem;
    }

    var initUserList = function(parent, parentTrigger, container, client) {
      var clients = [],
          clientContainer = $(container),
          parentContainer = $(parent),
          trigger = $(parentTrigger),
          shown = false;
      
      var userList = {
        isShown: function() {
          return shown;
        },
        show: function() {
          shown = true;
          parentContainer.animate({ 
            'duration' : 100,
            'right' : '-10px'
          });
          $(".userDetail").show();
          clientContainer.find(".dropdown-toggle").removeClass("disabled");
          trigger.show();
        },
        hide: function() {
          shown = false;
          parentContainer.animate({ 
            'duration' : 100,
            'right' : '-150px'
          }, function(){
            parentContainer.on("click", function(e) {
              parentContainer.unbind('click');
              userList.show();
              e.preventDefault();
            })
          });  
          $(".userDetail").hide();
          clientContainer.find(".dropdown-toggle").addClass("disabled");
          trigger.hide()
        },
        clear: function() {
          clients = [];
          clientContainer.empty();
          this.hide();
        },
        has: function(clientName) {
          return clients.indexOf(clientName) != -1;
        },
        add: function(clientInfo) {
          var elt = makeClientElt(clientInfo, client);
          if (!this.isShown()) {
            elt.find(".dropdown-toggle").addClass("disabled");
          } else {
            elt.find(".userDetail").show();
          }
          clientContainer.append(elt);
          clients.push(clientInfo.name);
        },
        replace: function(clients) {
          this.clear();
          var currentClient;
          for (var clientName in clients) {
            if (clients.hasOwnProperty(clientName)) {
              var clientInRoom = clients[clientName];
              var gravatar = clientInRoom["gravatar"],
                  tagLine = clientInRoom["tagLine"] || "",
                  otherLatitude = parseFloat(clientInRoom["latitude"]),
                  otherLongitude = parseFloat(clientInRoom["longitude"]);
              var clientInfo = {
                name: clientName,
                gravatar: gravatar,
                tagLine: tagLine,
                distance: distance(otherLatitude, otherLongitude, latitude, longitude).toFixed(2)
              }
              this.add(clientInfo);
            }
          }
        },
        refresh: function(room) {
          var that = this;
          GeoParty.backend.getClientsInRoom(nickName, room, function(clients) {
            that.replace(clients);
          });
        },
        remove: function(clientName) {
          clientContainer.find("li[data-client-name=" + clientName + "]").remove();
          var clientIndex = clients.indexOf(clientName);
          clients.splice(clientIndex, 1);
        }
      }
      trigger.click(function(e){
        e.preventDefault();
        if (userList.isShown()){
          userList.hide();
        };                
      }); 
      parentContainer.on("click", function(e) {
        if (!userList.isShown()) {
          parentContainer.unbind('click');
          userList.show();
        }
        e.preventDefault();
      })

      return userList;
    }

    var chatBox = function(chatboxSelector) {
      var chatBoxContainer = $(chatboxSelector);
      return {
        clear: function() {
          chatBoxContainer.empty();
        },
        append: function(elt) {
          var span = $("<span class='message'></span>");
          chatBoxContainer.append(span.html(elt));
          chatBoxContainer.append($("<br>"));
          span.get()[0].scrollIntoView();
        }
      }
    }

    var makeLabel = function(from, fromGravatar) {
      var label = $("<span class='label label-default'></span>");
      label.text(from)
      label.prepend("<img src='" + fromGravatar + "' class='gravatar-label'> ")
      return label;
    }

    var makeMsgFrom = function(from, fromGravatar, message) {
      var label = makeLabel(from, fromGravatar);
      var msg = $("<span></span").text(message).prepend(" ").prepend(label).html();
      return msg;
    }

    var makeSpecialEvent = function(message) {
      var msg = $("<span class='label label-primary'></span>").text(message);
      return msg;
    }

    var makeInputBox = function(inputForm, inputText, client) {
      var form = $(inputForm),
          text = $(inputText);        

      form.submit(function(e){
        var cmd = text.val(),
            parts = cmd.split(/[ \t]+/);
        if (parts[0] != null) {              
          if (parts[0][0] == "/") {
            switch(parts[0].slice(1)) {
              case "join":
              case "j":
                var roomToJoin = $.trim(parts[1]);
                client.joinRoom(roomToJoin);
                break;
              case "msg":
              case "m":
                var clientName = $.trim(parts[1])
                  , clientIndex = cmd.indexOf(parts[1])
                  , msg = $.trim(cmd.slice(clientIndex + parts[1].length));
                client.sendPrivateMessage(clientName, msg)
                break;
            case "leave":
            case "l":
                var room = $.trim(parts[1]);
                client.leaveRoom(room);
                break;

            }
          } else {
            var msg = $.trim(cmd);
            if (client.currentClient() != null) {
              client.sendPrivateMessage(client.currentClient(), msg);
            } else {
              client.sendRoomMessage(client.currentRoom(), msg);
            }
          }
        }
        text.val("");
        e.preventDefault();
      });

      return {
        "val" : function() {
          return text.val();
        },
        "setPlaceholder": function(placeholder) {
          text.attr("placeholder", placeholder);
        }
      }

    }

    var client = {
      init: function(success, failure) {            
        GeoParty.backend.register(nickName, email, tagLine, latitude, longitude, success, failure);            
      },          
      nickName: function(){
        return nickName;
      },
      tagLine: function(){
        return tagLine;
      },
      start: function() {
        this.roomComponent = initListComponent('#rooms', '#roomList', makeRoomItem, roomFinder, this);
        this.personComponent = initListComponent('#privates', '#privateList', makePrivateItem, privateFinder, this);
        this.fileTransferComponent = initListComponent('#files', '#fileList', makeFileItem, fileFinder, this, true);
        this.userListComponent = initUserList('#users', '#users-trigger', '#userList', this);            
        this.chatBox = chatBox('#chatbox');
        this.inputBox = makeInputBox('#input', '#text', this);
        var that = this;
        var mainRefresh = setInterval(function(){
          GeoParty.backend.getEvents(nickName, function(data) {
            $.each(data, function(idx, elt) {
              switch(elt.type) {
                case "join": 
                  that.joinedRoom(elt);
                  break;
                case "message":
                  that.receiveRoomMessage(elt);
                  break;
                case "leave":
                  that.leftRoom(elt);
                  break;
                case "privateMessage":
                  that.receivePrivateMessage(elt);
                  break;
                case "privateMessageSent":
                  that.sentPrivateMessage(elt);
                  break;
                case "fileTransferRequest":
                  that.receiveFile(elt);
                  break;
                case "subscribe":
                  that.subscribedRoom(elt);
                  break;
              }
            });
          });
        }, 1000);

      },
      currentRoom: function() {
        return currentRoom;
      },
      currentRoomName: function() {
        if (currentRoom == null) {
          return null;
        } else {
          return roomNames[currentRoom];
        }
      },
      currentClient: function() {
        return currentClient;
      },
      joinRoom: function(room, success, failure) {
        GeoParty.backend.joinRoom(nickName, room, success, failure);
      },
      leaveRoom: function(room) {
        GeoParty.backend.leaveRoom(nickName, room);
        delete roomNames[room]
      },
      relocate: function(latitude, longitude, success, failure) {
        GeoParty.backend.setNewLocation(nickName, latitude, longitude, success, failure);
      },
      sendPrivateMessage: function(clientName, msg) {
        GeoParty.backend.sendPrivateMessage(nickName, clientName, msg);
      },
      changeRoom: function(room) {
        if (currentInterval != null) {
          clearInterval(currentInterval);
        }
        currentRoom = room;
        currentClient = null;
        this.roomComponent.select(room);
        this.personComponent.loseFocus();
        this.chatBox.clear();
        var that = this;
        $.each(rooms[currentRoom], function(idx, elt) {
          that.chatBox.append(elt);
        });
        this.userListComponent.refresh(currentRoom);
        this.roomComponent.clearBadge(room);
        this.inputBox.setPlaceholder("Room: " + this.currentRoomName());
      },
      startPrivateMsg: function(client, clientInfo) {
        if (!this.personComponent.has(client)) {
          privates[client] = [];
          this.personComponent.add(client);
        }
        this.changeToPrivateMsg(client, clientInfo);
      },
      changeToPrivateMsg: function(client, clientInfo) {
        if (currentInterval != null) {
          clearInterval(currentInterval);
        }
        currentClient = client;
        currentRoom = null;
        this.personComponent.select(currentClient);
        this.roomComponent.loseFocus();
        this.chatBox.clear();
        var that = this;
        $.each(privates[currentClient], function(idx, elt) {
          that.chatBox.append(elt);
        });
        this.userListComponent.clear();
        this.userListComponent.add(clientInfo);
        this.personComponent.clearBadge(client);
        this.inputBox.setPlaceholder("Chat:" + this.currentClient());
      },
      sendRoomMessage: function(room, msg) {
        GeoParty.backend.sendRoomMessage(nickName, room, msg);
      },
      sendFile: function(target, file) {
        GeoParty.backend.sendFile(nickName, target, file, function(){
          var customXhr = new window.XMLHttpRequest();
          if(customXhr.upload){ 
            customXhr.upload.addEventListener('progress', function(e) {
              if (e.lengthComputable) {
                $('#progressBarContainer').show();
                var percentComplete = e.loaded / e.total * 100;
                $('#uploadProgress').css('widtgh', percentComplete + '%')
              } else {
                $('#progressBarContainer').hide();
                console.log("not computable");
              }
            },
            false);
            customXhr.upload.addEventListener('load', function(e) {
              console.log("Upload finished");
              $('#progressBarContainer').hide();
              $('#uploadProgress').css('width', '0%')                    
              $('#upload').modal('hide');
            },
            false);
          }
          return customXhr;
        });
      },
      joinedRoom: function(event) {
        var msg = makeSpecialEvent(event.from + " has joined " + event.roomPrettyName);
        createOrUpdate(rooms, event.room, msg);            
        if (currentRoom == event.room) {
          this.userListComponent.refresh(currentRoom);
          this.chatBox.append(msg);
        }
      },
      receiveRoomMessage: function(event) {
        var msg = makeMsgFrom(event.from, event.fromGravatar, event.message);
        createOrUpdate(rooms, event.room, msg);
        if (currentRoom != null && event.room == currentRoom) {
          this.chatBox.append(msg);
        } else {
          this.roomComponent.incBadge(event.room);
        }
      },
      leftRoom: function(event) {
        var msg = makeSpecialEvent(event.from + " has left " + event.roomPrettyName);
        createOrUpdate(rooms, event.room, msg);
        if (currentRoom == event.room) {
          this.userListComponent.remove(event.from);            
          this.chatBox.append(msg);
        }
      },
      receivePrivateMessage: function(event) {
        var msg = makeMsgFrom(event.from, event.fromGravatar, event.message);
        createOrUpdate(privates, event.from, msg);
        if (! this.personComponent.has(event.from) ) {
          this.personComponent.add(event.from, true);
        }
        if (currentClient != null && event.from == currentClient) {
          this.chatBox.append(msg);
        } else {
          this.personComponent.incBadge(event.from);
        }
      },
      sentPrivateMessage: function(event) {
        var msg = makeMsgFrom(nickName, event.fromGravatar, event.message);
        createOrUpdate(privates, event.to, msg);
        if (currentClient != null) {
          this.chatBox.append(msg);
        }
      },
      receiveFile: function(event) {
        transfers[event.id] = event;
        this.fileTransferComponent.add(event.id, true);
        this.fileTransferComponent.incBadge(event.id);
      }, 
      subscribedRoom: function(event) {
        var msg = makeSpecialEvent("You have joined " + event.roomPrettyName);
        createOrUpdate(rooms, event.room, msg);
        roomNames[event.room] = event.roomPrettyName;
        this.changeRoom(event.room);
        if (! this.roomComponent.has(event.room) ) {
          this.roomComponent.add(event.room);
        }
      }
    };

    return client;
  }
}