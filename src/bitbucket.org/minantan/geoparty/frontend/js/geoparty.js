if (!jQuery) { throw new Error("GeoParty requires jQuery") }

var GeoParty = GeoParty || {}
GeoParty.backend = {
  sendPrivateMessage: function(nickName, client, msg, success, failure) {              
    $.post("/client/private/" + nickName + "/" + client + "/", { text: msg }).done(success).fail(failure);
  },
  sendRoomMessage: function(nickName, room, msg, success, failure) {
    $.post("/client/say/" + nickName + "/" + room + "/", { text: msg }).done(success).fail(failure);
  },
  joinRoom: function(nickName, room, success, failure) {
    if (room == null) {
      $.get("/client/join/" + nickName + "/").done(success).fail(failure);              
    } else {
      $.get("/client/join/" + nickName + "/" + room + "/").done(success).fail(failure);              
    }
  },
  setNewLocation: function(nickName, latitude, longitude, success, failure) {
    $.get("/location/" + nickName + "/" + latitude + "/" + longitude + "/").done(success).fail(failure);              
  },
  leaveRoom: function(nickName, room) {
    $.get("/client/leave/" + nickName + "/" + room + "/").done(success).fail(failure);
  },
  getEvents: function(nickName, success, failure) {
    $.get("/events/" + nickName).done(success).fail(failure);
  },
  getClientInfo: function(nickName, success, failure) {
    $.get("/clientInfo/" + nickName).done(success).fail(failure);
  },
  sendFile: function(nickName, target, file, xhr, success, failure) {
    var data = new FormData();
    data.append('file', file[0].files[0]);

    var options = {
      url: "/upload/" + nickName +"/" + target,
      data: data,
      cache: false,
      contentType: false,
      processData: false,
      type: 'POST'
    };

    if (xhr) {
      options.xhr = xhr;
    }

    $.ajax(options).done(success).fail(failure);            
  },
  register: function(nickName, email, tagLine, latitude, longitude, success, failure) {
    $.post("/client/" + nickName, { email: email, info: tagLine, latitude : latitude, longitude : longitude }).done(success).fail(failure); 
  },
  getClientsInRoom: function(nickName, room, success, failure) {
    $.get("/clients/" + nickName + "/" + room).done(success).fail(failure);   
  },
  getRooms: function(latitude, longitude, distance, success, failure) {
    console.log("Getting rooms");
    var data = {}
    if (latitude && longitude) {
      data["lat"] = latitude;
      data["lng"] = longitude;
    }
    if (distance) {
      data["rad"] = distance;
    }
    $.get("/rooms/", data).done(success).fail(failure);   
  }
};