if (!GeoParty || !GeoParty.backend) { throw new Error("GeoParty Client requires GeoParty backend") }

GeoParty.client = {
  makeClient: function(nickName, email, tagLine, latitude, longitude) {
    var createOrUpdate = function(obj, key, item) {
      if (!(key in obj)) {
        obj[key] = [ item ];
      } else {
        obj[key].push(item);
      }            
    }


    var rooms = {},
        roomNames = {},
        privates = {},
        currentRoom = null,
        currentClient = null,
        currentInterval = null,
        transfers = {};

    var initListComponent = function(container, primaryList, secondaryContainer, secondaryList, itemMaker, itemFinder, client, allowLastRoomLeave) {
      var rooms = [],
          badges = {};          
      var roomContainer = $(container),
        roomList = $(primaryList),
        moreRoomListContainer = $(secondaryContainer),
        moreRoomList = $(secondaryList);

      moreRoomListContainer.hide();
      roomContainer.hide();

      var makePrimaryItem = function(room) {
        var item = itemMaker(room, client);
        item.addClass("selected");
        return item;
      }

      return {
        get: function() {
          return rooms;
        },
        has: function(room) {
          return rooms.indexOf(room) != -1;
        },
        add: function(room, doNotJoin) {              
          roomContainer.show();
          // assume: we always add and change room
          var primaryItem = doNotJoin ?
            itemMaker(room, client) : makePrimaryItem(room);

          if (rooms.length > 0) {
            moreRoomListContainer.show();

            if (doNotJoin) {
              var moreItem = itemMaker(room, client);
              moreRoomList.prepend(moreItem);
            } else {
              var selectedRoom = this.selected();
              itemFinder(roomContainer, selectedRoom).remove();
              roomList.prepend(primaryItem);

              var moreItem = itemMaker(selectedRoom, client);
              moreRoomList.prepend(moreItem);
            }

          } else {
            roomList.prepend(primaryItem);
          }
          rooms.unshift(room);
          console.log("add room called with " + room );
          if (!doNotJoin) {
            roomContainer.addClass("selected");
          }
          delete badges[room];
        },
        remove: function(room) {
          // can't leave the last room!
          if (rooms.length == 1 && !allowLastRoomLeave) {
            console.log("can't leave the last room!");
            return;
          }

          itemFinder(roomContainer, room).remove();              
          if (this.selected() == room && !allowLastRoomLeave) {
            var newRoom = rooms[1];
            var newPrimaryItem = makePrimaryItem(newRoom);
            roomList.prepend(newPrimaryItem);
          }
          rooms.splice(rooms.indexOf(room), 1);
          if (rooms.length <= 1) {
            moreRoomListContainer.hide();
          }
          delete badges[room];
        },
        select: function(room) {
          roomContainer.addClass("selected")

          if (room == this.selected()) {
            itemFinder(roomContainer, room).parent().addClass("selected");
            console.log("room already selected");
            return;
          }
          var roomIndex = rooms.indexOf(room);
          if (roomIndex == -1) {
            console.log("Cannot select a room you are not in");
            return;
          }

          var currentRoom = this.selected();

          itemFinder(roomContainer, room).remove();
          itemFinder(roomContainer, currentRoom).remove();

          var newPrimaryItem = makePrimaryItem(room);
          roomList.prepend(newPrimaryItem);

          var newSecondaryItem = itemMaker(currentRoom, client);
          moreRoomList.prepend(newSecondaryItem);

          rooms.splice(roomIndex, 1);
          rooms.unshift(room);


        },
        selected: function() {
          if (rooms.length > 0) {
            return rooms[0];
          } else {
            return null;
          }
        },
        loseFocus: function() {
          itemFinder(roomContainer, this.selected()).parent().removeClass("selected");
          roomContainer.removeClass("selected")
        },
        incBadge: function(room) {
          if (room in badges) {
            badges[room]++;
          } else {
            badges[room] = 1;
          }
          var badge = itemFinder(roomContainer, room).find(".badge");
          badge.text(badges[room]);
        },
        clearBadge: function(room){
          delete badges[room];
          var badge = itemFinder(roomContainer, room).find(".badge");
          badge.text("");
        }
      }
    }

    var makeRoomItem = function(room, client) {
      var listItem = $("<li></li>"),
          roomPrettyName = roomNames[room],
          link = $("<a href='#' id='room-" + room + "'><span class='glyphicon glyphicon-map-marker'></span>  " + roomPrettyName + "&nbsp;<span class='badge'></span></a>");

      link.click(function(e) {
        client.changeRoom(room);
      });              
      listItem.append(link);
      // listItem.append("blah blah");

      return listItem;
    }

    var roomFinder = function(context, room) {
      return context.find("#room-" + room);
    }

    var makeFileItem = function(fileId, client) {
      var transferEvent = transfers[fileId];
      var listItem = $("<li></li>"),          
          link = $("<a href='/download/" + transferEvent.id + "' id='file-" + transferEvent.id + "'><span class='glyphicon glyphicon-file'></span>  " + transferEvent.filename + " from " + transferEvent.from + "</a>");
      link.click(function(e){
        client.fileTransferComponent.remove(fileId);
        delete transfers[fileId];
      });
      listItem.append(link);

      return listItem;
    }

    var fileFinder = function(context, fileId) {
      return context.find("#file-" + fileId);
    }

    var makePrivateItem = function(clientName, client) {
      var listItem = $("<li></li>"),
          link = $("<a href='#' id='client-" + clientName + "'><span class='glyphicon glyphicon-user'></span> " + clientName + "&nbsp;<span class='badge'></span></a>");

      link.click(function(e) {
        var clientItem = $("li[data-client-name=" + clientName + "]");
          gravatar = clientItem.find(".gravatar"),
          tagLine = clientItem.find(".tagLine"),
          userDistance = clientItem.find(".userDistance");
        var clientInfo = {
          name: clientName, 
          gravatar: gravatar.attr("src"),
          tagLine: tagLine.text() || "",
          distance: userDistance.text() || ""
        };
        client.changeToPrivateMsg(clientName, clientInfo);
        e.preventDefault();
      });
      listItem.append(link);

      return listItem;
    }

    var privateFinder = function(context, client) {
      return context.find("#client-" + client);
    }

    var makeClientElt = function(clientInfo, client) {
      // "<img src='" + gravatar + "' class='gravatar pull-left' > "
      var listItem = $("<li data-client-name='" + clientInfo.name + "' class='dropdown media clearfix'></li>");
      var gravatarItem = $("<img src='" + clientInfo.gravatar + "' class='gravatar pull-left'>")
      if(clientInfo.name != nickName) {
        listItem.addClass('dropdown');

        // var dropdownTrigger = $("<a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-cog'></span></a>");

        var dropdownTrigger = $("<a class='dropdown-toggle pull-left' data-toggle='dropdown' href='#'></a>");
        dropdownTrigger.append(gravatarItem);
        listItem.prepend(dropdownTrigger);

        var dropdownMenu = $("<ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>" +
              "<li><a href='#' id='client-private-" + clientInfo.name + "'><span class='glyphicon glyphicon-user'></span> Private chat</a></li>" +
              "<li><a href='#' id='client-send-" + clientInfo.name + "'><span class='glyphicon glyphicon-upload'></span> Send file</a></li>" +
            "</ul>");
        listItem.append(dropdownMenu);
        listItem.find("#client-private-" + clientInfo.name).click(function(e) {
          client.startPrivateMsg(clientInfo.name, clientInfo);
          e.preventDefault();
        });
        listItem.find("#client-send-" + clientInfo.name).click(function(e) {
          $('#uploadTarget').val(clientInfo.name);
          $('#upload').modal();
          e.preventDefault();
        });
      } else {
        gravatarItem.addClass("pull-left")
        listItem.append(gravatarItem);
      }
      listItem.append($("<div class='media-body'><span class='media-heading'><strong>" + clientInfo.name + "</strong></span><br><span class='tagLine'>" + (clientInfo.tagLine ? clientInfo.tagLine : "") + "</span> </div>"));
      if (clientInfo.name != nickName && clientInfo.distance) {
        var distance = $("<span class='label label-default'><span class='userDistance'>" + clientInfo.distance + "</span> km <span class='glyphicon glyphicon-globe'></span></span>");
        listItem.find('.media-body').append(distance);
      }
      return listItem;
    }

    var initUserList = function(container, client) {
      var clients = [],
          clientContainer = $(container);
      return {
        clear: function() {
          clients = [];
          clientContainer.empty();
        },
        has: function(clientName) {
          return clients.indexOf(clientName) != -1;
        },
        add: function(clientInfo) {
          var elt = makeClientElt(clientInfo, client);
          clientContainer.append(elt);
          clients.push(clientInfo.name);
        },
        replace: function(clients) {
          console.log(this);
          this.clear();
          for (var clientName in clients) {
            if (clients.hasOwnProperty(clientName)) {
              var clientInRoom = clients[clientName];
              var gravatar = clientInRoom["gravatar"],
                  tagLine = clientInRoom["tagLine"] || "",
                  otherLatitude = parseFloat(clientInRoom["latitude"]),
                  otherLongitude = parseFloat(clientInRoom["longitude"]);
              var clientInfo = {
                name: clientName,
                gravatar: gravatar,
                tagLine: tagLine,
                distance: distance(otherLatitude, otherLongitude, latitude, longitude).toFixed(2)
              }
              this.add(clientInfo);
            }
          }
        },
        refresh: function(room) {
          var that = this;
          GeoParty.backend.getClientsInRoom(nickName, room, function(clients) {
            that.replace(clients);
          });
        },
        remove: function(clientName) {
          clientContainer.find("li[data-client-name=" + clientName + "]").remove();
          var clientIndex = clients.indexOf(clientName);
          clients.splice(clientIndex, 1);
        }
      }
    }

    var chatBox = function(chatboxSelector) {
      var chatBoxContainer = $(chatboxSelector);
      return {
        clear: function() {
          chatBoxContainer.empty();
        },
        append: function(elt) {
          var span = $("<span class='message'></span>");
          chatBoxContainer.append(span.html(elt));
          chatBoxContainer.append($("<br>"));
          span.get()[0].scrollIntoView();
        }
      }
    }

    var makeLabel = function(from, fromGravatar) {
      var label = $("<span class='label label-default'></span>");
      label.text(from)
      label.prepend("<img src='" + fromGravatar + "' class='gravatar-label'> ")
      return label;
    }

    var makeMsgFrom = function(from, fromGravatar, message) {
      var label = makeLabel(from, fromGravatar);
      var msg = $("<span></span").text(message).prepend(" ").prepend(label).html();
      return msg;
    }

    var makeSpecialEvent = function(message) {
      var msg = $("<span class='label label-primary'></span>").text(message);
      return msg;
    }

    var client = {
      init: function(success, failure) {            
        GeoParty.backend.register(nickName, email, tagLine, latitude, longitude, success, failure);            
      },          
      start: function() {
        this.roomComponent = initListComponent('#rooms', '#roomList', '#moreRoomListContainer', '#moreRoomList', makeRoomItem, roomFinder, this);
        this.personComponent = initListComponent('#privates', '#privateList', '#morePrivateListContainer', '#morePrivateList', makePrivateItem, privateFinder, this);
        this.fileTransferComponent = initListComponent('#files', '#fileList', '#moreFileListContainer', '#moreFileList', makeFileItem, fileFinder, this, true);
        this.userListComponent = initUserList("#userList", this);            
        this.chatBox = chatBox('#chatbox');
        var that = this;
        var mainRefresh = setInterval(function(){
          GeoParty.backend.getEvents(nickName, function(data) {
            console.log("data" + data);
            $.each(data, function(idx, elt) {
              switch(elt.type) {
                case "join": 
                  that.joinedRoom(elt);
                  break;
                case "message":
                  that.receiveRoomMessage(elt);
                  break;
                case "leave":
                  that.leftRoom(elt);
                  break;
                case "privateMessage":
                  that.receivePrivateMessage(elt);
                  break;
                case "privateMessageSent":
                  that.sentPrivateMessage(elt);
                  break;
                case "fileTransferRequest":
                  that.receiveFile(elt);
                  break;
                case "subscribe":
                  that.subscribedRoom(elt);
                  break;
              }
            });
          });
        }, 1000);

      },
      currentRoom: function(room) {
        return currentRoom;
      },
      currentClient: function(room) {
        return currentClient;
      },
      joinRoom: function(room) {
        GeoParty.backend.joinRoom(nickName, room);
      },
      leaveRoom: function(room) {
        GeoParty.backend.leaveRoom(nickName, room);
        delete roomNames[room]
      },
      sendPrivateMessage: function(clientName, msg) {
        GeoParty.backend.sendPrivateMessage(nickName, clientName, msg);
      },
      changeRoom: function(room) {
        if (currentInterval != null) {
          clearInterval(currentInterval);
        }
        currentRoom = room;
        currentClient = null;
        this.roomComponent.select(room);
        this.personComponent.loseFocus();
        this.chatBox.clear();
        var that = this;
        $.each(rooms[currentRoom], function(idx, elt) {
          that.chatBox.append(elt);
        });
        this.userListComponent.refresh(currentRoom);
        this.roomComponent.clearBadge(room);
      },
      startPrivateMsg: function(client, clientInfo) {
        if (!this.personComponent.has(client)) {
          privates[client] = [];
          this.personComponent.add(client);
        }
        this.changeToPrivateMsg(client, clientInfo);
      },
      changeToPrivateMsg: function(client, clientInfo) {
        if (currentInterval != null) {
          clearInterval(currentInterval);
        }
        currentClient = client;
        currentRoom = null;
        this.personComponent.select(currentClient);
        this.roomComponent.loseFocus();
        this.chatBox.clear();
        var that = this;
        $.each(privates[currentClient], function(idx, elt) {
          that.chatBox.append(elt);
        });
        this.userListComponent.clear();
        this.userListComponent.add(clientInfo);
        this.personComponent.clearBadge(client);
      },
      sendRoomMessage: function(room, msg) {
        GeoParty.backend.sendRoomMessage(nickName, room, msg);
      },
      sendFile: function(target, file) {
        GeoParty.backend.sendFile(nickName, target, file, function(){
          var customXhr = new window.XMLHttpRequest();
          if(customXhr.upload){ 
            customXhr.upload.addEventListener('progress', function(e) {
              if (e.lengthComputable) {
                $('#progressBarContainer').show();
                var percentComplete = e.loaded / e.total * 100;
                $('#uploadProgress').css('width', percentComplete + '%')
              } else {
                $('#progressBarContainer').hide();
                console.log("not computable");
              }
            },
            false);
            customXhr.upload.addEventListener('load', function(e) {
              $('#progressBarContainer').hide();
              $('#uploadProgress').css('width', '0%')                    
              $('#upload').modal('hide');
            },
            false);
          }
          return customXhr;
        });
      },
      joinedRoom: function(event) {
        var msg = makeSpecialEvent(event.from + " has joined " + event.roomPrettyName);
        createOrUpdate(rooms, event.room, msg);            
        if (currentRoom == event.room) {
          this.userListComponent.refresh(currentRoom);
          this.chatBox.append(msg);
        }
      },
      receiveRoomMessage: function(event) {
        var msg = makeMsgFrom(event.from, event.fromGravatar, event.message);
        createOrUpdate(rooms, event.room, msg);
        if (currentRoom != null && event.room == currentRoom) {
          this.chatBox.append(msg);
        } else {
          this.roomComponent.incBadge(event.room);
        }
      },
      leftRoom: function(event) {
        var msg = makeSpecialEvent(event.from + " has left " + event.roomPrettyName);
        createOrUpdate(rooms, event.room, msg);
        if (currentRoom == event.room) {
          this.userListComponent.remove(event.from);            
          this.chatBox.append(msg);
        }
      },
      receivePrivateMessage: function(event) {
        var msg = makeMsgFrom(event.from, event.fromGravatar, event.message);
        createOrUpdate(privates, event.from, msg);
        if (! this.personComponent.has(event.from) ) {
          this.personComponent.add(event.from, true);
        }
        if (currentClient != null && event.from == currentClient) {
          this.chatBox.append(msg);
        } else {
          this.personComponent.incBadge(event.from);
        }
      },
      sentPrivateMessage: function(event) {
        var msg = makeMsgFrom(nickName, event.fromGravatar, event.message);
        createOrUpdate(privates, event.to, msg);
        if (currentClient != null) {
          this.chatBox.append(msg);
        }
      },
      receiveFile: function(event) {
        transfers[event.id] = event;
        this.fileTransferComponent.add(event.id, true);
      }, 
      subscribedRoom: function(event) {
        var msg = makeSpecialEvent("You have joined " + event.roomPrettyName);
        createOrUpdate(rooms, event.room, msg);
        roomNames[event.room] = event.roomPrettyName;
        // appendLink(event.room, this);
        this.changeRoom(event.room);
        if (! this.roomComponent.has(event.room) ) {
          this.roomComponent.add(event.room);
        }
      }
    };

    return client;
  }
}