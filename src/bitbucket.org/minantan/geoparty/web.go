package main

import (
    "fmt"
    "errors"
    "strings"
    "strconv"
    "time"
    "encoding/json"
    "net/http"
    "io"
    // "io/ioutil"
    "bitbucket.org/minantan/geoparty/backend"
    "github.com/gorilla/sessions"
    // "code.google.com/p/go.net/websocket"

)

const (
  ClientTimeout = 720 * time.Hour
)

type AddClientRequest struct {
  name string
  client *backend.Client
  handler *backend.BufferingHandler
  resultChan chan *backend.RoomInfo
}

type GetClientRequest struct {
  name string
  responseChan chan *backend.Client
  errorChan chan error
}

type GetOtherClientRequest struct {
  name string
  responseChan chan chan backend.Event
  errorChan chan error
}

type ClientManager struct {
  clients map[string]*backend.Client
  add chan AddClientRequest
  get chan GetClientRequest
  getOther chan GetOtherClientRequest
}
func (cm *ClientManager) getClient(name string, clientFunc func(*backend.Client)) {
  response := make(chan *backend.Client)
  errorChan := make(chan error)
  cm.get <- GetClientRequest{ name, response, errorChan }
  go func() {
    select {
    case client := <- response:
      clientFunc(client)
    case err := <- errorChan:
      fmt.Println(err)
    }
  }()
}
func (cm *ClientManager) getClientSync(name string, clientFunc func(*backend.Client)) {
  response := make(chan *backend.Client)
  errorChan := make(chan error)
  cm.get <- GetClientRequest{ name, response, errorChan }
  // go func() {
    select {
    case client := <- response:
      clientFunc(client)
    case <- errorChan:      
      clientFunc(nil)
    }
  // }()
}
func (cm *ClientManager) hasClientSync(name string) bool {
  _, present := cm.clients[name]
  return present
  // }()
}


const (
  lenPath = len("/client/")
  lenJoinPath = len("/client/join/")
  lenLeavePath = len("/client/leave/")
  lenPrivatePath = len("/client/private/")
  lenSayPath = len("/client/say/")
  lenClientsPath = len("/clients/")
  lenShowPath = len("/show/")
  lenUploadPath = len("/upload/")  
  lenDownloadPath = len("/download/")  
  lenLocationPath = len("/location/")  
  lenEventsPath = len("/events/")  
)

var store = sessions.NewCookieStore([]byte("JKOSLCMSKDOFKDLSSDA87rfdancvxft1ojfiubrs"))

func ensureRegistered(delegate func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
  return func(w http.ResponseWriter, r *http.Request) {
    session, _ := store.Get(r, "geo-party")
    if session.IsNew {
      http.Error(w, "Not authenticated", http.StatusNotFound)      
      return
    } else {
      delegate(w, r)
    }
  }
}

func handlerMaker(manager ClientManager) func(w http.ResponseWriter, r *http.Request) {

  return func(w http.ResponseWriter, r *http.Request) {
    name := strings.TrimSpace(r.URL.Path[lenPath:])
    latitudeStr := r.FormValue("latitude")
    longitudeStr := r.FormValue("longitude")
    email := r.FormValue("email")
    info := r.FormValue("info")
    errors := make(map[string]string)

    latitude, latErr := strconv.ParseFloat(latitudeStr, 64)
    longitude, lonErr := strconv.ParseFloat(longitudeStr, 64) 

    if latErr != nil || lonErr != nil {
      http.Error(w, "Unable to parse location coordinates", http.StatusBadRequest)
      return
    }

    for _, c := range name {
      if !strings.ContainsRune(backend.RandomChars, c) {
        errors["chatName"] = fmt.Sprintf("Only alphanumeric characters are allowed, '%c' cannot be used", c)
        break
      }
    }
    if manager.hasClientSync(name) {
      errors["chatName"] = "Nickname already in use, try another?"
    }

    if len(errors) > 0 {
      errorJson, errs := json.Marshal(&errors)
      if (errs != nil) {
        http.Error(w, errs.Error(), http.StatusBadRequest)
      } else {
        w.Header().Add("Content-Type", "application/json")
        w.WriteHeader(http.StatusBadRequest)
        w.Write(errorJson)
      }
      return
    }


    clientHandler := backend.NewBufferingHandler()
    c := backend.ClientService(name, strings.ToLower(strings.TrimSpace(email)), strings.TrimSpace(info), clientHandler, latitude, longitude)
    joinedRoomChan := make(chan *backend.RoomInfo)
    manager.add <- AddClientRequest{ name, c, clientHandler, joinedRoomChan}
    joinedRoom := <- joinedRoomChan

    session, _ := store.Get(r, "geo-party")
    session.Values["nickName"] = c.GetClientInfo().GetName();
    session.Values["email"] = c.GetClientInfo().GetEmail();
    session.Values["info"] = c.GetClientInfo().GetInfo();
    session.Save(r, w)

    w.Header().Add("Content-Type", "application/json")
    result := map[string]string{ 
      "room": joinedRoom.GetName(), 
      "prettyName": joinedRoom.GetPrettyName(),
      "clientName": c.GetClientInfo().GetName(),
      "gravatarUrl": c.GetClientInfo().GetGravatarUrl(),
    }
    enc := json.NewEncoder(w)
    enc.Encode(&result)
  }
}

func joinHandlerMaker(server chan backend.SubRequest, manager ClientManager) func(w http.ResponseWriter, r *http.Request) {
  return ensureRegistered(func(w http.ResponseWriter, r *http.Request) {    
    frags := strings.FieldsFunc(r.URL.Path[lenJoinPath:], func(t rune) bool { return t == '/' })
    name := frags[0]
    room := ""
    if len(frags) > 1 {
      room = frags[1]
    }
    manager.getClient(name, func(client *backend.Client){
      fmt.Printf("%v\n", client.GetClientInfo())
      server <- backend.NewSubRequest(room, room == "", client.GetClientInfo(), nil)
    })
  })
}

func setNewLocationHandlerMaker(manager ClientManager) func(w http.ResponseWriter, r *http.Request) {
  return ensureRegistered(func(w http.ResponseWriter, r *http.Request) {    
    frags := strings.FieldsFunc(r.URL.Path[lenLocationPath:], func(t rune) bool { return t == '/' })
    name := frags[0]
    latitudeStr := frags[1]
    longitudeStr := frags[2]
    latitude, latErr := strconv.ParseFloat(latitudeStr, 64)
    longitude, lonErr := strconv.ParseFloat(longitudeStr, 64) 

    if latErr != nil || lonErr != nil {
      http.Error(w, fmt.Sprintf("Unable to parse location coordinates - %s, %s", latErr, lonErr), http.StatusBadRequest)
      return
    }

    manager.getClientSync(name, func(client *backend.Client){
      client.GetClientInfo().Latitude = latitude
      client.GetClientInfo().Longitude = longitude
    })
  })
}

func leaveHandlerMaker(server chan backend.SubRequest, manager ClientManager) func(w http.ResponseWriter, r *http.Request) {
  return ensureRegistered(func(w http.ResponseWriter, r *http.Request) {
    frags := strings.FieldsFunc(r.URL.Path[lenLeavePath:], func(t rune) bool { return t == '/' })
    name, room := frags[0], frags[1]
    manager.getClient(name, func(client *backend.Client){
      client.LeaveRoom(room)
    })
  })
}

func listClientsInRoomHandlerMaker(manager ClientManager) func(w http.ResponseWriter, r *http.Request) {
  return ensureRegistered(func(w http.ResponseWriter, r *http.Request) {
    frags := strings.FieldsFunc(r.URL.Path[lenClientsPath:], func(t rune) bool { return t == '/' })
    name, room := frags[0], frags[1]
    manager.getClientSync(name, func(client *backend.Client){
      if r, present := client.GetRoom(room); !present {
        fmt.Printf("%s is not in room %s\n", name, room)
      } else {
        clients := r.GetClients()

        clientNames := make(map[string](map[string]string))
        for clientName, client := range clients {
          clientNames[clientName] = map[string]string{
            "gravatar" : client.GetGravatarUrl(),
            "tagLine" : client.GetInfo(),
            "latitude" : fmt.Sprintf("%f", client.Latitude),
            "longitude" : fmt.Sprintf("%f", client.Longitude),
          }
        }
        w.Header().Add("Content-Type", "application/json")
        enc := json.NewEncoder(w)
        enc.Encode(&clientNames)
      }
    })
  })
}

func showEventsHandlerMaker(manager ClientManager) func(w http.ResponseWriter, r *http.Request) {
  return ensureRegistered(func(w http.ResponseWriter, r *http.Request) {
    frags := strings.FieldsFunc(r.URL.Path[lenEventsPath:], func(t rune) bool { return t == '/' })
    name := frags[0]
    manager.getClientSync(name, func(client *backend.Client){
      if (client == nil) {
        http.Error(w, fmt.Sprintf("Could not find %s", name), http.StatusNotFound)
        return
      }
      handler := client.GetHandler()
      response := make(chan []map[string]string)
      handler.(*backend.BufferingHandler).GetEvents(response)
      buffer := <- response
      client.GetClientInfo().SetActive(true);
      w.Header().Add("Content-Type", "application/json")
      enc := json.NewEncoder(w)
      enc.Encode(buffer)
    })
  })
}

func sayHandlerMaker(manager ClientManager) func(w http.ResponseWriter, r *http.Request) {
  return ensureRegistered(func(w http.ResponseWriter, r *http.Request) {
    frags := strings.FieldsFunc(r.URL.Path[lenSayPath:], func(t rune) bool { return t == '/' })
    name, room := frags[0], frags[1]
    text := r.FormValue("text")
    manager.getClient(name, func(client *backend.Client){
      client.SendRoomMessage(room, text)
    })
  })
}

func clientInfoHandlerMaker(manager ClientManager) func(w http.ResponseWriter, r *http.Request) {
  return ensureRegistered(func(w http.ResponseWriter, r *http.Request) {
    frags := strings.FieldsFunc(r.URL.Path[lenSayPath:], func(t rune) bool { return t == '/' })
    name := frags[0]
    manager.getClientSync(name, func(client *backend.Client){
      if (client == nil) {
        http.Error(w, fmt.Sprintf("Could not find %s", name), http.StatusNotFound)        
        return        
      }      
      clientInfo := client.GetClientInfo()
      clientInfoJson := map[string]string{
        "gravatar" : clientInfo.GetGravatarUrl(),
        "tagLine" : clientInfo.GetInfo(),
        "latitude" : fmt.Sprintf("%f", clientInfo.Latitude),
        "longitude" : fmt.Sprintf("%f", clientInfo.Longitude),
      }
      w.Header().Add("Content-Type", "application/json")
      enc := json.NewEncoder(w)
      enc.Encode(&clientInfoJson)

    })
  })
}


func privateHandlerMaker(server chan backend.SubRequest, manager ClientManager) func(w http.ResponseWriter, r *http.Request) {
  return ensureRegistered(func(w http.ResponseWriter, r *http.Request) {
    frags := strings.FieldsFunc(r.URL.Path[lenPrivatePath:], func(t rune) bool { return t == '/' })
    name, to := frags[0], frags[1]
    text := r.FormValue("text")
    manager.getClient(name, func(client * backend.Client){
      getOtherChannel := func(to string, success func(chan backend.Event), fail func(error)) {
        otherResponse := make(chan chan backend.Event)
        otherError := make(chan error)
        go func() {
          manager.getOther <- GetOtherClientRequest{ to, otherResponse, otherError }
          select {
          case resp := <- otherResponse:
            success(resp)
          case err := <- otherError:
            fail(err)
          }
        }()
      }        
      client.SendPrivateMessage(to, text, getOtherChannel)
    });
  })
}

func reconnect(clientManager *ClientManager) func(w http.ResponseWriter, r *http.Request) {
  return func(w http.ResponseWriter, r *http.Request) {
    session, _ := store.Get(r, "geo-party")
    if session.IsNew {
      http.Error(w, "Could not find previous session", http.StatusNotFound)
      return
    }

    nickName := session.Values["nickName"].(string)
    clientManager.getClientSync(nickName, func(client *backend.Client){
      if (client == nil) {
        http.Error(w, fmt.Sprintf("Could not find %s", nickName), http.StatusNotFound)
        return
      }
      rooms := client.GetRooms()
      roomValues := make(map[string]string)
      clientNames := client.GetClientNames()

      for k,v := range rooms {
        roomValues[k] = v.GetPrettyName()
      }

      priorData := map[string]interface{}{
        "nickName": client.GetClientInfo().GetName(),
        "email": client.GetClientInfo().GetEmail(),
        "info": client.GetClientInfo().GetInfo(),
        "gravatarUrl": client.GetClientInfo().GetGravatarUrl(),
        "rooms": roomValues,
        "clients": clientNames,
      }

      session.Save(r, w)
      w.Header().Add("Content-Type", "application/json")
      encoder := json.NewEncoder(w)
      encoder.Encode(&priorData)

    })
  }
}

func clientManager(server chan backend.SubRequest) ClientManager {
  clients := make(map[string]*backend.Client)
  addClientChan := make(chan AddClientRequest)
  getClientChan := make(chan GetClientRequest)
  getOtherClientChan := make(chan GetOtherClientRequest)
  manager := ClientManager{ clients, addClientChan, getClientChan, getOtherClientChan }
  go func(){
    for {
      select {
        case get := <- getClientChan:
          go func() {
            if c, present := clients[get.name]; present {
              get.responseChan <- c
            } else {
              get.errorChan <- errors.New("Client not found")
            }
          }()
        case otherGet := <- getOtherClientChan:
          go func() {
            if c, present := clients[otherGet.name]; present {
              clientInfo := c.GetClientInfo()
              otherGet.responseChan <- clientInfo.GetChannel()
            } else {
              otherGet.errorChan <- errors.New("Other Client not found")
            }
          }()
        case add := <- addClientChan:          
          fmt.Printf("Added %s\n", add.name)
          clients[add.name] = add.client
          go func() {            
            client := &add.client
            clientInfo := client.GetClientInfo()

            for {
              time.Sleep(ClientTimeout)              
              if clientInfo.IsActive() != true {                
                fmt.Printf("Killing %s due to inactivity\n", clientInfo.GetName())
                delete(clients, add.name)
                client.Kill()
                break
              } else {
                clientInfo.SetActive(false)
              }
            }
          }()

          server <- backend.NewSubRequest("", false, add.client.GetClientInfo(), add.resultChan)
      }
    }
  }()
  return manager
}

func downloadHandler(transferManager *backend.TransferManager) func(w http.ResponseWriter, r *http.Request) {
  return func(w http.ResponseWriter, r *http.Request) {
    frags := strings.FieldsFunc(r.URL.Path[lenDownloadPath:], func(t rune) bool { return t == '/' })
    id := frags[0]
    inputChannel := transferManager.GetChannel(id)
    if inputChannel == nil {
      http.Error(w, "no such request", http.StatusNotFound)
      return
    } 
    fileName := transferManager.GetFileName(id)
    headers := w.Header()
    headers.Add("Content-Disposition", fmt.Sprintf("attachment; filename=%s", fileName))
    for chunk := range inputChannel {
      w.Write(chunk)
    }
  }
}


func roomsHandler(server *backend.Server) func(w http.ResponseWriter, r *http.Request) {
  return func(w http.ResponseWriter, r *http.Request) {
    rooms := server.GetRooms()
    queryParams := r.URL.Query()
    lat,latPresent := queryParams["lat"]
    lng,lngPresent := queryParams["lng"]
    rad,radPresent := queryParams["rad"]

    var (
      latitude float64
      longitude float64
      radius float64
      latErr error
      lngErr error
      radErr error
      coordSpecified bool = false
    )
    if latPresent && lngPresent && len(lat) > 0 && len(lng) > 0 {
      latitude, latErr = strconv.ParseFloat(lat[0], 64)
      longitude, lngErr = strconv.ParseFloat(lng[0], 64)
      coordSpecified = true
    }
    if radPresent {
      radius, radErr = strconv.ParseFloat(rad[0], 64)
    } 
    if radErr != nil || !radPresent {
      radius = 5.0
    }

    w.Header().Add("Content-Type", "application/json")
    encoder := json.NewEncoder(w)
    roomsJson := make([]map[string]interface{}, len(rooms))
    i := 0
    for _, v := range rooms {
      roomInfo := v.GetRoom()
      if coordSpecified && latErr == nil && lngErr == nil {
        if backend.Distance(latitude, longitude, roomInfo.GetLatitude(), roomInfo.GetLongitude()) > radius {
          continue
        }
      }
      roomsJson[i] = map[string]interface{}{
        "name": roomInfo.GetName(),
        "prettyName": roomInfo.GetPrettyName(),
        "latitude": roomInfo.GetLatitude(),
        "longitude": roomInfo.GetLongitude(),
      }
      i++
    }
    roomsJson = roomsJson[0:i]

    encoder.Encode(roomsJson)
  }
}

func uploadHandler(transferManager *backend.TransferManager, clientManager ClientManager) func(w http.ResponseWriter, req *http.Request) {
  return func(w http.ResponseWriter, r *http.Request) {
    frags := strings.FieldsFunc(r.URL.Path[lenUploadPath:], func(t rune) bool { return t == '/' })
    from, to := frags[0], frags[1]
    mr, err := r.MultipartReader()
    if err != nil {
      http.Error(w, "not a form", http.StatusBadRequest)                                          
      return
    }
    defer r.Body.Close()                                                                                

    inChan := make(chan []byte)
    for {
      part, err := mr.NextPart()
      if err != nil {
        if err == io.EOF {
          break
        }
        http.Error(w, "bad form part", http.StatusBadRequest)                                   
        return
      } else {

        fmt.Println("filename: ", part.FileName())                                                  
        fmt.Println("formname: ", part.FormName())
        if (part.FormName() == "file") {
          id := transferManager.Register(part.FileName(), inChan)
          // get user
          clientManager.getClientSync(to, func(client *backend.Client) {
            if (client == nil) {
              http.Error(w, fmt.Sprintf("Could not find %s", to), http.StatusNotFound)
              return
            }

            fmt.Printf("id is %s\n", id)
            go func() {
              client.GetClientInfo().GetFileTransferChan() <- backend.NewPendingRequest(id, from, part.FileName())
            }()

            for {
              buffer := make([]byte, 100000)
              bytes, err := part.Read(buffer)
              if bytes < 100000 {
                newBuffer := make([]byte, bytes)
                copy(newBuffer, buffer)
                buffer = newBuffer
              }
              if err == io.EOF {
                close(inChan)
                transferManager.Finish(id);
                break
              }
              select {
                case inChan <- buffer:
                case <- time.After(2 * time.Minute):
                  fmt.Println("Transfer timed out");
                  close(inChan)
                  transferManager.Finish(id);
                  http.Error(w, "Unable to send file, transfer timed out.", http.StatusBadRequest)
                  return;
              }
            }
          })    
        }
      }
    }
  }
}

// func wsGeoParty)(ws *websocket.Conn) {
//     for {
//       buffer := make([]byte, 100000)
//       bytes, err := conn.Read(buffer)
//       if bytes < 100000 {
//         newBuffer := make([]byte, bytes)
//         copy(newBuffer, buffer)
//         buffer = newBuffer
//       }
//       if err == io.EOF {
//         close(inChan)
//         break
//       }
//       inChan <- buffer 
//     }
// }

func main() {
    server := backend.ServerService()
    manager := clientManager(server.GetControl())
    transferManager := backend.NewTransferManager()    
    // http.HandleFunc("/hook/register/", joinHandlerMaker(server.GetControl(), manager))
    http.HandleFunc("/download/", downloadHandler(transferManager))
    http.HandleFunc("/upload/", uploadHandler(transferManager, manager))
    http.HandleFunc("/events/", showEventsHandlerMaker(manager))
    http.HandleFunc("/rooms/", roomsHandler(server))
    http.HandleFunc("/clients/", listClientsInRoomHandlerMaker(manager))
    http.HandleFunc("/clientInfo/", clientInfoHandlerMaker(manager))
    http.HandleFunc("/location/", setNewLocationHandlerMaker(manager))
    http.HandleFunc("/existing", reconnect(&manager))
    http.HandleFunc("/client/say/", sayHandlerMaker(manager))
    http.HandleFunc("/client/private/", privateHandlerMaker(server.GetControl(), manager))
    http.HandleFunc("/client/join/", joinHandlerMaker(server.GetControl(), manager))
    http.HandleFunc("/client/leave/", leaveHandlerMaker(server.GetControl(), manager))
    http.HandleFunc("/client/", handlerMaker(manager))
    http.Handle("/", http.FileServer(http.Dir("frontend")))
    // http.Handle("/geo", websocket.Handler(wsGeoParty))
    http.ListenAndServe(":8080", nil)
}
